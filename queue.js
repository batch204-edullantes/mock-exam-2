let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    collection = collection.filter((value, index) => {
        if(index != 0) {
            return value;
        }
    });
    console.log(collection);
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    console.log(collection.length);
    return collection.length ;
}

function isEmpty() {
    return collection.length == 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};